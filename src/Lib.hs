module Lib where

import Data.ByteString.Short.Internal (ShortByteString, toShort)
import Data.ByteString.Internal (packChars)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

toShortByteString :: String -> ShortByteString
toShortByteString = toShort . packChars
