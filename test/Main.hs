module Main where

import Test.HUnit

import ParserSpec
import EmitSpec
import ModuleSpec



main :: IO Counts
main = do
--  runTestTT testsParser
--  runTestTT testsCodegen
  runTestTT testsModule
