module ParserSpec where

import Test.HUnit
import MyParser (parseToplevel)
import Syntax


testParseFunction :: Test
testParseFunction = TestCase (assertEqual "parse function" (parseToplevel "def f(a) a;") (Right [Function "f" ["a"] (Var "a")]))


testsParser :: Test
testsParser = TestList [testParseFunction]