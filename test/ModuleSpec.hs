{-# LANGUAGE OverloadedStrings #-}
module ModuleSpec where

import Codegen (double, emptyModule)

import Test.HUnit

import qualified LLVM.AST as AST
import           LLVM.AST hiding (Module)
import qualified LLVM.AST.Constant as C
import           LLVM.AST.Global
import           LLVM.AST.Type hiding (double)

import           LLVM.Context
import           LLVM.Exception
import           LLVM.Module

import           Control.Exception
import LLVM.AST.Float ( SomeFloat(Double))

ast :: AST.Module
ast = defaultModule
        { moduleDefinitions =
            [ GlobalDefinition
                functionDefaults
                { name = "test"
                , returnType = void
                , basicBlocks =
                    [ BasicBlock
                        "entry"
                        [ UnName 0 := Alloca i32 Nothing 0 []
                        , -- UnName 1 :=
                          Do $ Store
                            False
                            (LocalReference (ptr i32) (UnName 0))
                            (ConstantOperand (C.Int 32 42))
                            Nothing
                            0
                            []
                        ]
                        (Do $ Ret Nothing [])
                    ]
                }
            ]
        }


call :: IO AST.Module
call = withContext $ \context ->
  withModuleFromAST context ast $ \m -> return ast

testWithModuleFromAST::Test
testWithModuleFromAST = TestCase $ do
  actual <- call
  assertEqual "test module" actual (emptyModule "empty")


testsModule :: Test
testsModule = TestList [testWithModuleFromAST]

