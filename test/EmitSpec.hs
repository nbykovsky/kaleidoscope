{-# LANGUAGE TupleSections #-}
module EmitSpec where

import Test.HUnit
import MyParser
import Syntax
import Codegen
import Emit
import Control.Monad.State (state, runState)


func = Function "f" ["a"] (Var "a")

emptymod = emptyModule "empty"

testCodegenFunction :: Test
testCodegenFunction =
  TestCase $ do
    expected <- codegen (emptyModule "empty") [func]
    assertEqual "codegen function" expected (emptyModule "empty")


testCodegenTopFunction::Test
testCodegenTopFunction = TestCase $ assertEqual "codegenTop function" (runLLVM emptymod (codegenTop func)) emptymod
-- ((emptyModule "empty"))

testsCodegen :: Test
testsCodegen = TestList [testCodegenFunction]